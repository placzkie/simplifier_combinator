package simplifier

import java.util

import AST._

import scala.Int

// to implement
// avoid one huge match of cases
// take into account non-greedy strategies to resolve cases with power laws
object Simplifier {


  def simplify(node: Node): Node = {
    node match {
        case node: NodeList => simplifyNodeList(node)
        case node: BinExpr => simplifyBinExp(node)
        case node: IntNum => node
        case node: Variable => node
        case node: TrueConst => node
        case node: FalseConst => node
        case node: Unary => simplifyUnary(node)
        case node: KeyDatumList => simplifyKeyDatumList(node)
        case node: ElemList => node
        case node: Assignment => simplifyAssignment(node)
        case node: WhileInstr => simplifyWhile(node)
        case node: IfElifInstr => simplifyIfElifElse(node)
        case node: Tuple => node
        case e => e
    }
  }

  def simplifyNodeList(node: NodeList): Node = {
    node match {
      case NodeList(List(x:Assignment, y:Assignment)) if x.left == y.left => y

      case _ => handleNodeList(node)
    }
  }

  def handleNodeList(node: NodeList) = {
    var resultList: List[Node] = List()

    for(n <- node.list) {
      val tempResult = simplify(n)
      tempResult match {
        case EmptyNode() =>
        case NodeList(List()) =>
        case NodeList(List(x:NodeList)) => resultList = resultList ++ x.list//elif
        case x:NodeList => resultList = resultList ++ x.list
        case  _ => resultList = resultList :+ tempResult
      }
    }

    NodeList(resultList)
  }

  def simplifyUnary(node: Unary): Node = {
    node match {
      case Unary("not", Unary("not", Unary("not", x))) => Unary("not", x)
      case Unary("-", Unary("-", x)) => x

        //get rid of not before comparisons
      case Unary("not", BinExpr("==", x, y)) => BinExpr("!=", x, y)
      case Unary("not", BinExpr("!=", x, y)) => BinExpr("==", x, y)
      case Unary("not", BinExpr(">", x, y)) => BinExpr("<=", x, y)
      case Unary("not", BinExpr("<", x, y)) => BinExpr(">=", x, y)
      case Unary("not", BinExpr(">=", x, y)) => BinExpr("<", x, y)
      case Unary("not", BinExpr("<=", x, y)) => BinExpr(">", x, y)

        //evaluate constants
      case Unary("not", FalseConst()) => TrueConst()
      case Unary("not", TrueConst()) => FalseConst()
      case e => e
    }
  }

  def simplifyKeyDatumList(node: KeyDatumList) = {
    val l2 = node.list.groupBy(_.key).map(x => x._2.reverse.head).toList
    KeyDatumList(l2)
  }

  def simplifyAssignment(node: Assignment): Node = {
    node match {
      case Assignment(x, y) if x == y => EmptyNode()
      case Assignment(x, y:IfElseExpr) => Assignment(x, simplifyIfElseExpr(y))
      case e =>  e
    }
  }

  def simplifyWhile(node: WhileInstr) = {
    node match {
      case WhileInstr(FalseConst(), _) => EmptyNode()
    }
  }

  def simplifyIfElifElse(node: IfElifInstr) = {
    node match {
      case IfElifInstr(TrueConst(), x, _, _)  => x
      case IfElifInstr(FalseConst(), _, _, y)  => y
    }
  }

  def simplifyIfElseExpr(node: IfElseExpr) = {
    node match {
      case IfElseExpr(TrueConst(), left, _) => left
      case IfElseExpr(FalseConst(), _, right) => right
    }
  }

  def handleTuples(node: Node) = node match {
      case BinExpr("+", x: Tuple, y: Tuple) => Tuple(x.list ++ y.list)
      case _ => handleEvaluation(node)
    }

  def handlePowerLaws(node: Node) = node match {
          case BinExpr("*", BinExpr("**", x, y), BinExpr("**", w, z)) if x == w => BinExpr("**", x, BinExpr("+", y, z))
          case BinExpr("**", x, y:IntNum) if y.value == 0 => IntNum(1)
          case BinExpr("**", x, y:IntNum) if y.value == 1 => x
          case BinExpr("**", x:FloatNum, y:FloatNum) => FloatNum(Math.pow(x.value, y.value))
          case BinExpr("**", x:IntNum, y:IntNum) => IntNum(Math.pow(x.value.floatValue(), y.value.floatValue()).toInt)
          case BinExpr("**", x:IntNum, y) => simplify(BinExpr("**", x, simplify(y)))
          case BinExpr("**", x, y:IntNum) => (BinExpr("**", simplify(x), y))
          case BinExpr("**", BinExpr("**", x, n), m) => BinExpr("**", x, BinExpr("*", n, m))
          case BinExpr("+", BinExpr("+", BinExpr("**", x, y:IntNum), BinExpr("*", BinExpr("*", v:IntNum, w), z)), BinExpr("**", a, b:IntNum))
            if y.value == 2 && v.value == 2 && b.value == 2 && x == w && z == a => BinExpr("**", BinExpr("+", x, z), IntNum(2))
          case BinExpr("-", BinExpr("-", BinExpr("**", BinExpr("+", x, y), z:IntNum), BinExpr("**", u, v:IntNum)), BinExpr("*", BinExpr("*", a:IntNum, b), c))
            if x == u && z.value == 2 && v.value == 2 && a.value == 2 && b == x => BinExpr("**", c, IntNum(2))
          case BinExpr("-", BinExpr("**", BinExpr("+", x, y), z:IntNum), BinExpr("**", BinExpr("-", a, b), c:IntNum))
            if z.value == 2 && x == a && y == b && c.value == 2 => BinExpr("*", BinExpr("*", IntNum(4), x), y)

          case _ => handleTuples(node)
  }

  def handleEvaluation(node: Node) = node match {
    case BinExpr("+", x:IntNum, y:IntNum) => IntNum(x.value + y.value)
    case BinExpr("-", x:IntNum, y:IntNum) => IntNum(x.value - y.value)
    case BinExpr("*", x:IntNum, y:IntNum) => IntNum(x.value * y.value)
    case BinExpr("/", x:IntNum, y:IntNum) => IntNum(x.value / y.value)
    case BinExpr("+", x:IntNum, y) => simplify(BinExpr("+", x, simplify(y)))
    case BinExpr("+", x, y:IntNum) => simplify(BinExpr("+", simplify(x), y))
    case BinExpr("*", x:IntNum, y) => simplify(BinExpr("*", x, simplify(y)))
    case BinExpr("*", x, y:IntNum) => simplify(BinExpr("*", simplify(x), y))
    case BinExpr("-", x:IntNum, y) => simplify(BinExpr("-", x, simplify(y)))
    case BinExpr("-", x, y:IntNum) => simplify(BinExpr("-", simplify(x), y))
    case BinExpr("%", x:IntNum, y) => simplify(BinExpr("%", x, simplify(y)))
    case BinExpr("%", x, y:IntNum) => simplify(BinExpr("%", simplify(x), y))

    case _ => handleCommutativity(node)
  }

  def handleDivision(node: Node) =  node match {
    case BinExpr("/", x, y) if x == y => IntNum(1)
    case BinExpr("/", BinExpr("+", x, BinExpr("*", y, z)), BinExpr("+", u, BinExpr("*", v, w)))
      if x == u && y == v && z == w => IntNum(1)
    case BinExpr("/", BinExpr("+", x, y), BinExpr("+", v, z)) if x == z && y == v => IntNum(1)
    case BinExpr("/", BinExpr("+", x, BinExpr("*", y, z)), BinExpr("+", BinExpr("*", u, v), w))
      if x == w && y == u && z == v => IntNum(1)
    case BinExpr("/", x:IntNum, BinExpr("/", y:IntNum, z)) if x.value == 1 && y.value == 1 => z
    case BinExpr("/", x:IntNum, BinExpr("/", y:IntNum, BinExpr("-", z, v)))
      if x.value == 1 && y.value == 1 => BinExpr("-", z, v)
    case BinExpr("*", x, BinExpr("/", y:IntNum, z)) if y.value == 1 => BinExpr("/", x, z)

    case BinExpr("/", x:IntNum, y) => simplify(BinExpr("/", x, simplify(y)))
    case BinExpr("/", x, y:IntNum) => simplify(BinExpr("/", simplify(x), y))

    case _ => handlePowerLaws(node)

  }

  def handleSimplifyExpressions(node: Node) = node match {
    case BinExpr("+", x, y:IntNum) if y.value == 0 => x
    case BinExpr("+", y:IntNum, x) if y.value == 0 => x
    case BinExpr("-", x, y) if x.toStr == y.toStr => IntNum(0)
    case BinExpr("*", x, y:IntNum) if y.value == 1 => x
    case BinExpr("*", x:IntNum, y) if x.value == 1 => y
    case BinExpr("*", x:IntNum, y) if x.value == 0 => IntNum(0)
    case BinExpr("+", Unary("-", x), y) if x.toStr == y.toStr => IntNum(0)
    case BinExpr("or", x, y) if x.toStr == y.toStr => x
    case BinExpr("and", x, y) if x.toStr == y.toStr => x
    case BinExpr("or", x, TrueConst()) => TrueConst()
    case BinExpr("or", x, FalseConst()) => x
    case BinExpr("and", x, FalseConst()) => FalseConst()
    case BinExpr("and", x, TrueConst()) => x
    case BinExpr("==", x, y) if x.toStr == y.toStr => TrueConst()
    case BinExpr(">=", x, y) if x.toStr == y.toStr => TrueConst()
    case BinExpr("<=", x, y) if x.toStr == y.toStr => TrueConst()
    case BinExpr("!=", x, y) if x.toStr == y.toStr => FalseConst()
    case BinExpr(">", x, y) if x.toStr == y.toStr => FalseConst()
    case BinExpr("<", x, y) if x.toStr == y.toStr => FalseConst()

    case _ => handleDivision(node)
  }

  def handleCommutativity(node: Node) = node match {
    case BinExpr("-", BinExpr("+", x, y), z) if x == z => y
    case BinExpr("and", BinExpr("or", a, b), BinExpr("or", c, d))
      if a == d && b == c => BinExpr("or", a, b)
    case BinExpr("or", BinExpr("and", a, b), BinExpr("and", c, d))
      if a == d && b == c => BinExpr("and", a, b)

    case _ => handleDistributivePropertyOfMultiplication(node)

  }

  def handleDistributivePropertyOfMultiplication(node: Node) = node match {
    case BinExpr("-", BinExpr("*", a:IntNum, x), y) if a.value == 2 && x == y => x
    case BinExpr("+", BinExpr("*", x, z), BinExpr("*", y, w))
      if z == w => BinExpr("*", BinExpr("+", x, y), z)
    case BinExpr("+", BinExpr("*", x, y), BinExpr("*", z, w))
      if x == z => BinExpr("*", x, BinExpr("+", y, w))
    case BinExpr("+", BinExpr("+", BinExpr("+", BinExpr("*", x, y), BinExpr("*", z, v)), BinExpr("*", a, b)), BinExpr("*", c, d))
      if x == z && y == b && v == d && a == c => BinExpr("*", BinExpr("+", x, a), BinExpr("+", y, d))

    case _ => handleListConcatenation(node)
  }

  def handleListConcatenation(node: Node) = node match {
    case BinExpr("+", x:ElemList, y:ElemList) if x.list.isEmpty && y.list.isEmpty => ElemList(List())
    case BinExpr("+", x:ElemList, y:ElemList) if y.list.isEmpty => x
    case BinExpr("+", x:ElemList, y:ElemList) if x.list.isEmpty => y
    case BinExpr("+", x:ElemList, y:ElemList) => ElemList(x.list ++ y.list)

    case _ => handleOther(node)
  }

  def handleOther(node: Node) = node match {
    case e => e
  }

  def simplifyBinExp(binExpr: BinExpr) = {
    handleSimplifyExpressions(binExpr)
  }
}
